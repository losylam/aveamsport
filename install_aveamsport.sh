#!/bin/sh

echo ""
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "= Téléchargement des sources  ="
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo ""

wget https://www.laurent-malys.fr/telechargement/toussportifs-aveam-src.zip

echo ""
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "= Extraction des sources      ="
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo ""

unzip toussportifs-aveam-src.zip

mkdir -p $HOME/aveamsport
cp -r toussportifs-aveam-src/aveamsport-*/* $HOME/aveamsport

cd $HOME/aveamsport

echo ""
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo "= Installation                ="
echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo ""

sh install.sh

exit 0
