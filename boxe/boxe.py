import requests

from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
import eventlet

from gpiozero import Button
rpi_ok = True

try:
    import RPi.GPIO
except (RuntimeError, ModuleNotFoundError):
    rpi_ok = False


app = Flask(__name__)
app.config['SECRET_KEY'] = "c'est un secret!"
socketio = SocketIO(app)

##################
# Configuration ##
##################

debug = False

pin_1 = 19
pin_2 = 20
pin_3 = 21


##################
# Flask         ##
##################


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/button')
def button():
    val = request.args.get("val")
    print("emit val: ", val)
    socketio.emit("button", data=val)
    return "ok"


@socketio.on('message')
def handle_message(message):
    print('received message: ',)
    print(message)
    emit('message', 'hello from flask')


##################
# GPIO          ##
##################


def send_1():
    pin = "1"
    print("sent_button", pin)
    requests.get('http://localhost:5000/button', params={"val": pin})


def send_2():
    pin = "2"
    print("sent_button", pin)
    requests.get('http://localhost:5000/button', params={"val": pin})


def send_3():
    pin = "3"
    print("sent_button", pin)
    requests.get('http://localhost:5000/button', params={"val": pin})


if __name__ == "__main__":
    if rpi_ok:
        button1 = Button(pin_1)
        button1.when_pressed = send_1
        button2 = Button(pin_2)
        button2.when_pressed = send_2
        button3 = Button(pin_3)
        button3.when_pressed = send_3

    socketio.run(app, debug=False)
