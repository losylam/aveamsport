$( document ).ready(function() {
    console.log('hello');
    var socket = io();
    socket.on('connect', function(){
        socket.emit('message', {data: 'hello io'});
    });


    //////////////////////
    // Configuration    //
    //////////////////////

    var options_lang = ["Français", "English", "Nederlands"];

    var options_level = {FR: ["Difficile", undefined, "Facile"],
                         EN: ["Hard", undefined, "Easy"],
                         NL: ["Moeilijk", undefined, "Gemakkelijk"]};

    var textes = {"FR": "Frappe les couleurs le plus rapidement possible",
                  "EN": "Hit the colours as fast as you can",
                  "NL": "Raak de kleuren snel"};

    var timeout_inactiv = 10;
    var timeout_boxe = 10;
    var timeout_question = 10;
    var timeout_score = 6;

    var questions = {
        "FLGE_Question_1_Possibilite_1_Difficile_EN.png": 3,
        "FLGE_Question_1_Possibilite_1_Difficile_FR.png": 3,
        "FLGE_Question_1_Possibilite_1_Difficile_NL.png": 3,
        "FLGE_Question_1_Possibilite_1_Facile_EN.png": 3,
        "FLGE_Question_1_Possibilite_1_Facile_FR.png": 3,
        "FLGE_Question_1_Possibilite_1_Facile_NL.png": 3,
        "FLGE_Question_1_Possibilite_2_Difficile_EN.png": 2,
        "FLGE_Question_1_Possibilite_2_Difficile_FR.png": 2,
        "FLGE_Question_1_Possibilite_2_Difficile_NL.png": 2,
        "FLGE_Question_1_Possibilite_2_Facile_EN.png": 2,
        "FLGE_Question_1_Possibilite_2_Facile_FR.png": 2,
        "FLGE_Question_1_Possibilite_2_Facile_NL.png": 2,
        "FLGE_Question_1_Possibilite_3_Difficile_EN.png": 3,
        "FLGE_Question_1_Possibilite_3_Difficile_FR.png": 3,
        "FLGE_Question_1_Possibilite_3_Difficile_NL.png": 3,
        "FLGE_Question_1_Possibilite_3_Facile_EN.png": 2,
        "FLGE_Question_1_Possibilite_3_Facile_FR.png": 2,
        "FLGE_Question_1_Possibilite_3_Facile_NL.png": 2,
        "FLGE_Question_1_Possibilite_4_Difficile_EN.png": 2,
        "FLGE_Question_1_Possibilite_4_Difficile_FR.png": 2,
        "FLGE_Question_1_Possibilite_4_Difficile_NL.png": 2,
        "FLGE_Question_1_Possibilite_4_Facile_EN.png": 3,
        "FLGE_Question_1_Possibilite_4_Facile_FR.png": 3,
        "FLGE_Question_1_Possibilite_4_Facile_NL.png": 3,
        "FLGE_Question_2_Possibilite_1_Difficile_EN.png": 1,
        "FLGE_Question_2_Possibilite_1_Difficile_FR.png": 1,
        "FLGE_Question_2_Possibilite_1_Difficile_NL.png": 1,
        "FLGE_Question_2_Possibilite_1_Facile_EN.png": 3,
        "FLGE_Question_2_Possibilite_1_Facile_FR.png": 3,
        "FLGE_Question_2_Possibilite_1_Facile_NL.png": 3,
        "FLGE_Question_3_Possibilite_1_Difficile_EN.png": 1,
        "FLGE_Question_3_Possibilite_1_Difficile_FR.png": 2,
        "FLGE_Question_3_Possibilite_1_Difficile_NL.png": 2,
        "FLGE_Question_3_Possibilite_1_Facile_EN.png": 1,
        "FLGE_Question_3_Possibilite_1_Facile_FR.png": 2,
        "FLGE_Question_3_Possibilite_1_Facile_NL.png": 2,
        "FLGE_Question_3_Possibilite_2_Difficile_EN.png": 3,
        "FLGE_Question_3_Possibilite_2_Difficile_FR.png": 3,
        "FLGE_Question_3_Possibilite_2_Difficile_NL.png": 2,
        "FLGE_Question_3_Possibilite_2_Facile_EN.png": 3,
        "FLGE_Question_3_Possibilite_2_Facile_FR.png": 3,
        "FLGE_Question_3_Possibilite_2_Facile_NL.png": 1,
    };


    //////////////////////
    // Init             //
    //////////////////////

    var state = "waiting_lang";

    var langs = ["FR", "EN", "NL"];
    var lang = "FR";
    var level = undefined;
    var options_col = ["red", "yellow", "blue"];

    var num_question = 1;
    var score = 0;
    var score_boxe = 0;
    var num_pos = [0, 0, 0];

    var id_question;

    var youpi = $("#youpi");
    var button = $(".button");
    var question = $(".question");
    var overlay = $(".overlay");
    var chrono = $(".chrono");
    var chrono_content = $(".chrono-content");
    var clock = document.getElementById("clock");
    var text_content = $(".container-text .text-content");
    var score_div = $(".container-score");
    var score_title = $(".score-title");
    var score_content = $(".container-score .score");
    var score_boxe_content = $(".container-score .score-boxe");

    var clock_rad = 370;
    var clock_stroke = 18;

    clock.setAttribute("width", clock_rad);
    clock.setAttribute("height", clock_rad);

    var clock_ctx = clock.getContext("2d");
    clock_ctx.lineWidth = clock_stroke;
    clock_ctx.strokeStyle = '#1559a0';
    clock_ctx.lineCap = "round";

    var clock_question = document.getElementById("clock-question");
    var clock_question_content = $(".clock-question-content");
    var clock_question_rad = 170;
    var clock_question_stroke = 10;

    clock_question.setAttribute("width", clock_question_rad);
    clock_question.setAttribute("height", clock_question_rad);

    var clock_question_ctx = clock_question.getContext("2d");
    clock_question_ctx.lineWidth = clock_question_stroke;
    clock_question_ctx.strokeStyle = '#1559a0';
    clock_question_ctx.lineCap = "round";

    var level_timeout;
    var question_interval;
    var question_timeout;


    //////////////////////
    // Socket com       //
    //////////////////////


    socket.on('message', function(data){
        console.log(data);
    });

    socket.on('perdu', function(data){
        console.log("perdu");
        bascule = true;

        if (state == "chrono"){
            end_chrono();
        }
    });

    socket.on('ok', function(data){
        console.log("ok");
        bascule = true;
    });

    function btn_action(data){
        console.log("button", data);
        if (state == "waiting_lang"){
            choix_langue(data);
        }else if(state == "waiting_level"){
            choix_level(data);
        }else if(state == "waiting_question"){
            reponse(data);
        }else if (state == "countdown"){
            add_score_boxe();
        }
    }
    socket.on('button', btn_action);


    //////////////////////
    // Test/debug       //
    //////////////////////

    $('body').click(function(){
        show($('.admin'));
    });

    $('.btn-admin-1').click(() => btn_action("1"));
    $('.btn-admin-2').click(() => btn_action("2"));
    $('.btn-admin-3').click(() => btn_action("3"));


    //////////////////////
    // Utils            //
    //////////////////////

    function show(frame){
        frame.animate({opacity: 1});
    }

    function hide(frame){
        frame.animate({opacity: 0});
    }

    function pos_rep(){
        if (num_question == 2){
            $(".btn-1").css("top", "865px")
                .css("left", "583px");
            $(".btn-2").css("top", "865px")
                .css("left", "885px");
            $(".btn-3").css("top", "865px")
                .css("left", "1187px");
        }else if (num_question == 3){
            if (level == "Difficile"){
                $(".btn-1").css("top", "360px")
                    .css("left", "1500px");
                $(".btn-2").css("top", "565px")
                    .css("left", "1500px");
                $(".btn-3").css("top", "775px")
                    .css("left", "1500px");
            }else{
                $(".btn-1").css("top", "795px")
                    .css("left", "531px");
                $(".btn-2").css("top", "795px")
                    .css("left", "870px");
                $(".btn-3").css("top", "795px")
                    .css("left", "1209px");
            }
        }else{
            $(".btn-1").css("top", "612px")
                .css("left", "557px");
            $(".btn-2").css("top", "612px")
                .css("left", "890px");
            $(".btn-3").css("top", "612px")
                .css("left", "1220px");
        }
    }

    function draw_arc(ctx, rad, stroke, angle){
        let r0 = rad/2;
        let r = (rad-stroke)/2;
        ctx.clearRect(0, 0, rad, rad);
        ctx.beginPath();
        ctx.arc(r0, r0, r, -Math.PI/2,  (-Math.PI/2)+angle%(2*Math.PI));
        ctx.stroke();
    }

    Math.lerp = function (value1, value2, amount) {
	      amount = amount < 0 ? 0 : amount;
	      amount = amount > 1 ? 1 : amount;
	      return value1 + (value2 - value1) * amount;
    };


    function add_score_boxe(){
        score_boxe += 1;
        chrono_content.html(score_boxe);
    }
    //////////////////////
    // Scenes           //
    //////////////////////

    function init(){
        state = "waiting_lang";
        console.log("init");

        hide(score_div);
        hide(question);
        hide(youpi);

        // show_score();
        // show(score_div);

        score = 0;
        score_boxe = 0;

        num_question = 1;
        num_pos[0] = (num_pos[0] + 1) % 4;
        num_pos[2] = (num_pos[2] + 1) % 2;

        pos_rep();

        $('.cln').css("opacity", 1);
        show(button);

        for (var i = 0; i < 3; i++){
            $('.cln-'+options_col[i]+' .elem').html(options_lang[i]);
        }
    }
    init();

    function choix_langue(num){
        state = "choix_lang";

        var lang_num = parseInt(num)-1;
        lang = langs[lang_num];

        for (var i = 0; i<3; i++){
            if (lang_num != i){
                hide($('.cln-' + options_col[i]));
            }
        }

        setTimeout(function(){
            hide(button);
        }, 1500);

        setTimeout(init_level, 2000);
    }

    function init_level(){
        state = "waiting_level";
        for (var i = 0; i < 3; i++){
            $('.cln-'+options_col[i]+' .elem').html(options_level[lang][i]);
            if (options_level[lang][i]){
                $('.cln-'+options_col[i]).css("opacity", 1);
            }else{
                $('.cln-'+options_col[i]).css("opacity", 0);
            }
        }
        show(button);

        level_timeout = setTimeout(init, timeout_inactiv * 1000);
    }

    function choix_level(num){
        level = undefined;

        clearTimeout(level_timeout);

        if (num == "1") {
            level = "Difficile";
            hide($('.cln-blue'));
        }
        if (num == "3") {
            level = "Facile";
            hide($('.cln-red'));
        }

        if (level){
            state = "countdown";
            setTimeout(start_countdown, 1500);
        }
    }


    function start_countdown(){
        state = "countdown";

        text_content.html(textes[lang]);
        text_content.removeClass(["fr", "en", "nl"]);
        text_content.addClass(lang.toLowerCase());


        hide(button);
        show(chrono);
        show(text_content);
        show(youpi);
        hide(overlay);

        chrono_content.html(score_boxe);
        var start_time = new Date();
        countdown_interval = setInterval(function(){
            var now = new Date() - start_time;
            draw_arc(clock_ctx, clock_rad, clock_stroke, now*Math.PI*2/(timeout_boxe*1000.0));
        }, 50);

        setTimeout(function(){
            clearInterval(countdown_interval);
            clock_ctx.clearRect(0, 0, clock_rad, clock_rad);
            start_question();
        }, timeout_boxe * 1000);
    }


    function start_question(){

        var num_pos_loc = num_pos[num_question - 1] + 1;
        id_question = "FLGE_Question_" + num_question + "_Possibilite_" + num_pos_loc + "_" + level + "_" + lang + ".png";

        question.css("background-image", "url(/static/images/" + id_question);

        pos_rep();
        hide(text_content);
        hide(chrono);
        hide(youpi);
        show(question);

        setTimeout(function(){state = "waiting_question";},  700);

        chrono_content.html(score_boxe);
        var start_time = new Date();
        question_interval = setInterval(function(){
            var now = new Date() - start_time;
            draw_arc(clock_question_ctx, clock_question_rad, clock_question_stroke, now*Math.PI*2/(timeout_question*1000.0));
            cur_time = Math.floor(now/1000);

            clock_question_content.html(" ");
            //clock_question_content.html(timeout_question - cur_time);
        }, 50);

        question_timeout = setTimeout(function(){
            reponse("0");
        }, timeout_question * 1000);
    }

    function reponse(data){
        state = "reponse";

        clearInterval(question_interval);
        clearTimeout(question_timeout);

        let user_choice = parseInt(data);
        let good_choice = questions[id_question];

        $('.overlay .btn-rep').css("background-image", "none");

        if (user_choice == 0){
            $('.btn-rep').css("background-image", "url(static/images/nok.png");
            $('.btn-rep').animate({"opacity": 1}, 1500);
        }else{
            if (user_choice == good_choice){
                score += 1;
                $('.btn-'+user_choice).css("background-image", "url(static/images/ok.png");
            }else{
                $('.btn-'+user_choice).css("background-image", "url(static/images/nok.png");
                $('.btn-'+good_choice).css("background-image", "url(static/images/ok.png");
                $('.btn-'+good_choice).css("opacity", 0);
                // show($('.btn-'+good_choice));
                $('.btn-'+good_choice).animate({"opacity": 1}, 1500);
            }
        }
        overlay.css("opacity", 1);
        setTimeout(next_question, 1500);
    }


    function next_question(){
        num_question += 1;
        clock_question_ctx.clearRect(0, 0, clock_question_rad, clock_question_rad);

	      console.log(num_question);
        if (num_question >= 4){
            show_score();
        }else{
            hide(question);
            show(chrono);
            start_countdown();
        }
    }

    function show_score(){
        score_title.html("Score");
        score_content.html(score + "/3");
        score_boxe_content.html(score_boxe);
        show(score_div);
        hide(overlay);
        hide(question);
         setTimeout(init, timeout_score * 1000);
        score = 0;
    }
});
