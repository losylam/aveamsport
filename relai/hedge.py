import sys
import requests
import math
from time import sleep

from marvelmind import MarvelmindHedge

from config import step


def read_cur_dist(filename):
    with open(filename, "r") as f:
        val = float(f.read())
        return val


def send(p):
    requests.get('http://localhost:5000/update', params=p)


def log_line(log_file, pos1, pos2):
    line = "<line "
    line += 'x1="%4.3f" y1="%4.3f" ' % (pos1[1], pos1[2])
    line += 'x2="%4.3f" y2="%4.3f" ' % (pos2[1], pos2[2])
    line += "/>\n"

    with open(log_file, "a") as f:
        f.write(line)


def dist(pos1, pos2):
    return math.sqrt((pos2[1] - pos1[1])**2 + (pos2[2] - pos1[2])**2)


def process():
    # create MarvelmindHedge thread
    hedge = MarvelmindHedge(tty="/dev/ttyACM0", adr=None, debug=False)

    last_pos = []

    log = True
    log_file = "log.svg"

    # if not os.path.exists(cur_dist_file):
    #     with open(cur_dist_file, "w") as f:
    #         f.write("0.0")
    # tot_dist = read_cur_dist(cur_dist_file)

    # params = {'distance': round(tot_dist, 2),
    #           'x': 0,
    #           'y': 0}
    # send(params)

    if (len(sys.argv) > 1):
        hedge.tty = sys.argv[1]
    hedge.start()
    while True:
        try:
            sleep(0.5)
            cur_pos = hedge.position()
            # print(cur_pos)
            if last_pos == []:
                last_pos = cur_pos

            cur_dist = dist(cur_pos, last_pos)

            # print("cur dist: %2.3f" % cur_dist)

            if cur_dist > step:
                if log:
                    log_line(log_file, last_pos, cur_pos)
                last_pos = cur_pos

                print('new dist: ', round(cur_dist, 2))
                params = {'distance': round(cur_dist, 2),
                          'x': cur_pos[1],
                          'y': cur_pos[2]}
                send(params)

        except KeyboardInterrupt:
            hedge.stop()  # stop and close serial port
            sys.exit()


if __name__ == "__main__":
    process()
