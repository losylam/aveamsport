import os

from config import cur_dist_file

from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit


app = Flask(__name__)
app.config['SECRET_KEY'] = "c'est un secret!"
socketio = SocketIO(app)

app.config['distance'] = 0


def read_cur_dist(filename):
    with open(filename, "r") as f:
        val = round(float(f.read()), 2)
        return val


@app.route('/')
def index():
    if not os.path.exists(cur_dist_file):
        with open(cur_dist_file, "w") as f:
            f.write("0.0")
    app.config['distance'] = read_cur_dist(cur_dist_file)

    return render_template('index.html', dist=app.config['distance'])


@app.route('/path')
def path():
    return render_template('path.html')


@app.route('/update', methods=["GET", 'POST'])
def update():
    app.config["distance"] += float(request.args["distance"])
    with open(cur_dist_file, "w") as f:
        f.write(str(round(app.config["distance"], 2)))

    socketio.emit("hedge", data={"distance": app.config["distance"]})
    return "ok"


@socketio.on('update')
def handle_update(message):
    print('received message: ',)
    print(message)

    app.config["distance"] = float(message["distance"])
    with open(cur_dist_file, "w") as f:
        f.write(str(round(app.config["distance"], 2)))


@socketio.on('message')
def handle_message(message):
    print('received message: ',)
    print(message)
    emit('message', 'hello from flask')


# def update_hedge():
#     print("update")
#     # while True:
#     cur_pos = hedge.position()
#     # socketio.emit("message", {'x': cur_pos[2]})
#     with app.app_context():
#         socketio.emit("hedge", "hello from hedge")
#     print(cur_pos)
#     time.sleep(0.5)
#     update_hedge()

if __name__ == '__main__':
    socketio.run(app, debug=False)
