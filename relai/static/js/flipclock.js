function create_num(n){
    var num = $('<ul/>', {
        class: "flip flip-" + n,
    });

    if ((n-1)%3 == 0){
        num.addClass("separator");
    }
    for (var i = 0 ; i<10; i++){
        var li = $('<li/>').appendTo(num);
        var aa = $('<a/>', {href: "#"}).appendTo(li);
        var up = $('<div/>', {class: "up"}).appendTo(aa);
        $('<div/>', {class: "shadow"}).appendTo(up);
        $('<div/>', {class: "inn"}).html(i).appendTo(up);

        var down = $('<div/>', {class: "down"}).appendTo(aa);
        $('<div/>', {class: "shadow"}).appendTo(down);
        $('<div/>', {class: "inn"}).html(i).appendTo(down);
    }

    return num;
}

function create_flip(elem, n){
    for (var i = 0; i < n; i++){
        create_num(n-i).appendTo(elem);
    }

    $('<span/>', {class: "unit"}).html("m").appendTo(elem);
}

function flip_play(num) {
    // $("body").removeClass("play");
    var aa = $("ul.flip-" + num + " li.active");

    if (aa.html() == undefined) {
        aa = $("ul.flip-" + num + " li").eq(0);
        aa.addClass("before")
            .removeClass("active")
            .next("li")
            .addClass("active")
            .closest("body")
            .addClass("play");

    }
    else if (aa.is(":last-child")) {
        $("ul.flip-" + num + " li").removeClass("before");
        aa.addClass("before").removeClass("active");
        aa = $("ul.flip-" + num + " li").eq(0);
        aa.addClass("active")
            .closest("body")
            .addClass("play");
    }
    else {
        $("ul.flip-" + num + " li").removeClass("before");
        aa.addClass("before")
            .removeClass("active")
            .next("li")
            .addClass("active")
            .closest("body")
            .addClass("play");
    }

}

function get_digits(num, l){
    var digits = [];
    var num_s = num.toString();
    for (var i = 0; i<num_s.length; i++){
        digits.push(parseInt(num_s.charAt(i)));
    }
    digits = digits.reverse();
    if (digits.length < l){
        var dif = l - digits.length;
        for (var j=0; j<dif; j++){
            digits.push(0);
        }
    }
    return digits;
}

function Flip_digit(pos, num){
    this.num = num;
    this.pos = pos;
    this.num_target = 0;

    this.flip_one = function(){
        this.num = (this.num + 1) % 10;
        flip_play(this.pos+1);
        if (this.num != this.num_target){
            setTimeout(()=>this.flip_one(), 100);
        }
    };

    this.flip = function(new_num){
        if (new_num != this.num_target){
            this.num_target = new_num;
            this.flip_one();
        }
    };

    return this;
}

function Flip_clock(n_digits, num){
    this.n_digits = n_digits;
    this.num = num;
    this.flip_digits = [];

    for(var i = 0; i<n_digits; i++){
        this.flip_digits.push(new Flip_digit(i,0));
    }

    this.flip = function(){
        this.num += 1;
        var digits = get_digits(this.num, this.n_digits);
        digits.forEach((val, pos) => {
            if (pos < this.n_digits){
                this.flip_digits[pos].flip(val);
            }
        });
    };

    this.flip_num = function(num){
        this.num = num;
        get_digits(this.num, this.n_digits).forEach((val, pos) => {
            if (pos < this.n_digits){
                this.flip_digits[pos].flip(val);
            }
        });
    };

    this.flip_num(num);
    return this;
}
