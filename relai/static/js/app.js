$( document ).ready(function(){
    console.log('hello');
    var socket = io();

    var state = "idle";

    //////////////////////
    // CONFIG           //
    //////////////////////

    // distance pendant laquelle la page affichant une destination atteinte reste
    // (en m. affiché à l'écran)
    var thresh_dist = 5000;

    // durée d'affichage de chaque page quand elle alterne avec le compteur
    // (en secondes)
    var page_duration = 9;


    // durée d'affichage de chaque page quand elle alterne avec le compteur
    // (en secondes)
    var lang_duration = 9;


    // durée d'activité avant que la page d'accueil ne s'affiche, en alternance avec la distance
    // (en secondes)
    var idle_duration = 10;

    var debug = false;

    // propriété graphiques ring
    var ring_margin = 100;
    var ring_radius = 100;
    var ring_offset = 2000;



    //////////////////////
    // DATA             //
    //////////////////////

    var textes = {
        depart: {
            fr: "<p>Ça y est&#8239;! La course est lancée&#8239;!</p><p>  Nous aurons besoin de tout le monde pour relever ce défi des 26 640 km pour faire un tour de la Terre&#8239;!</p>",
            en: "<p>You've made it! The race is on! </p><p> We'll need every single one of you to achieve our goal of travelling 16,553 miles around the world!</p>",
            nl: "<p>Het is zover! De koers is vertrokken! </p><p>  We zullen iedereen nodig hebben als we de uitdaging om de 26.640 km lange rit om de wereld te rijden, willen aangaan!</p>"
        },

        etape: {
            fr: "Bravo, vous avez parcourus %dist Km <br/> et vous êtes arrivés à %place&#8239;!",
            en: "Well done, you've completed %dist miles <br/> and reached %place!",
            nl: "Bravo, jullie hebben %dist km afgelegd <br/> en zijn in %place, aangekomen!"
        },

        arrivee: {
            fr: "Incroyable&#8239;! Vous avez tous ensemble réussi à parcourir les 26 640 km pour boucler le tour de la Terre. On y serait pas arrivés sans vous. Tous ensemble, tous sportifs&#8239;!",
            en: "Amazing! Together you've managed to complete 16,553 miles and travel all the way round the world. We couldn't have done it without you. All together, we're all athletes!",
            nl: "Ongelofelijk, jullie zijn er allemaal samen in geslaagd om de 26.640 km af te leggen en dus de wereld rond te rijden. Zonder jullie was dat nooit geluckt. Allemaal samen, allemaal sportief!"
        },
    };

    var milestone = [
        [739, "Berlin, en Allemagne", "Berlin, Germany", "Berlijn, Duitsland"],
        [992, "Vienne, en Autriche", "Vienna, Austria", "Wenen, Oostenrijk"],
        [1206, "Budapest, en Hongrie", "Budapest, Hungary", "Boedapest, Hongarije"],
        [1509, "Bucarest, en Roumanie", "Bucarest, Romania", "Boekarest, Roemenië"],
        [1900, "Kiev, en Ukraine", "Kiev, Ukraine", "Kiev, Oekraïne"],
        [4621, "Astana, au Kazakhstan", "Astana, Kazakhstan", "Astana, Kazakhstan"],
        [6872, "Oulan-Bator, en Mongolie", "Ulaanbaatar, Mongolia", "Oelan-Bator, Mongolië"],
        [8042, "Pékin, en Chine", "Beijing, China!", "Beijing, China"],
        [9533, "Tokyo, au Japon", "Tokyo, Japan!", "Tokio, Japan"],
        [17224, "Seattle, dans l’état de Washington aux Etats-Unis", "Seattle, in the state of Washington, USA", "Seattle, in de Amerikaanse staat Washington"],
        [20545, "Toronto, au Canada", "Toronto, Canada", "Toronto, Canada"],
        [21048, "Montréal, au Canada", "Montreal, Canada", "Montreal, Canada"],
        [22656, "St Johns, en Terre-Neuve", "St. John’s, in Newfoundland", "St. Johns, Newfoundland"],
    ];

    var milestone_data = [
        [0,     0     , "Villeneuve D'Ascq", "Villeneuve D'Ascq", "Villeneuve D'Ascq"],
        [739,   1500  , "Berlin", "Berlin", "Berlijn"],
        [992,   2000  , "Vienne", "Vienna", "Wenen"],
        [1206,  2500  , "Budapest", "Budapest", "Boedapest"],
        [1509,  3000  , "Bucarest", "Bucarest", "Boekarest"],
        [1900,  4000  , "Kiev", "Kiev", "Kiev"],
        [4621,  7000  , "Astana", "Astana", "Astana"],
        [6872,  8200  , "Oulan-Bator", "Ulaanbaatar", "Oelan-Bator"],
        [8042,  9000  , "Pékin", "Beijing", "Beijing"],
        [9533,  9800  , "Tokyo", "Tokyo", "Tokio"],
        [17224, 17000 , "Seattle", "Seattle", "Seattle"],
        [20545, 20500 , "Toronto", "Toronto", "Toronto"],
        [21048, 21000 , "Montréal", "Montreal", "Montreal"],
        [22656, 22500 , "St Johns", "St. John’s", "St. Johns"],
        [26640, 26640 , "", "", ""],
    ];

    var max_dist = 26640000;

    //////////////////////
    // INIT             //
    //////////////////////

    var cur_dist = Math.round(parseFloat($("#cur-dist").text())*10);
    console.log("start dist", cur_dist);
    var place = undefined;
    var cur_place = 0;
    var inter_place;
    var play_place = false;

    var one_turn = false;
    if (cur_dist > max_dist + 100){
        one_turn = true;
    }
    var last_hedge = 0;
    var idle = true;
    var dist_idle = 0;

    var current_audio = undefined;

    var container_admin = $(".container-admin");

    create_flip($('.flipclock'), 8);
    var flip = new Flip_clock(8, cur_dist);

    var container_ring = $("#container-ring");
    var ring = Ring("container-ring", "ring-canvas", ring_margin, ring_radius, ring_offset, milestone_data);

    ring.init();
    ring.create_labels();
    ring.draw_ring(0, 0);

    //////////////////////
    // TEXT             //
    //////////////////////

    var cur_lang = 0;
    var langs = ["fr", "en", "nl"];
    var lang_interval = undefined;

    function create_text_etape(num, lang){
        var out = textes["etape"][lang];
        var dist = milestone[num-1][0];
        var dist_str =  new Intl.NumberFormat('fr-FR').format(dist);
        if (lang == "en"){
            dist_str =  new Intl.NumberFormat('en-EN').format(Math.round(dist*0.621371));
        }else if(lang == "nl"){
            dist_str =  new Intl.NumberFormat('nl-NL').format(dist);
        }
        out = out.replace("%dist", dist_str);
        out = out.replace("%place", milestone[num-1][1 + ["fr", "en", "nl"].indexOf(lang)]);

        return out;
    }

    function create_text(elem, txt){
        $(elem).html("");
        for (const [key, item] of Object.entries(txt)){
            $("<div/>", {class: key + " text-item"}).html(item).appendTo(elem);
        }

        $(".fr").css("opacity", 1);
        $(".en").css("opacity", 0);
        $(".nl").css("opacity", 0);

        start_lang_interval(true);
    }

    function start_lang_interval(val){
        clearInterval(lang_interval);
        if (val){
            lang_interval = setInterval(function(){
                cur_lang = (cur_lang + 1)%3;
                langs.forEach((l, i) => {
                    if (i != cur_lang){
                        hide($("."+l));
                    }else{
                        show($("."+l));
                    }
                });
            }, lang_duration * 1000);
        }
    }

    function create_text_place(elem){
        $(elem).html("");
        var txt = {};
        langs.forEach((l) => txt[l] = create_text_etape(place, l));
        return txt;
    }


    //////////////////////
    // Utils            //
    //////////////////////


    function show(frame){
        frame.animate({opacity: 1});
    }

    function hide(frame){
        frame.animate({opacity: 0});
    }

    function play(filename){
        tip_audio = new Audio(filename);
        tip_audio.play();
        return tip_audio;
    }

    function easeInOutQuad(x){
        return x < 0.5 ? 2 * x * x : 1 - Math.pow(-2 * x + 2, 2) / 2;
    }


    // hide($('.container-text'));

    //////////////////////
    // ADMIN            //
    //////////////////////


    $("body").click(function(){
        if (container_admin.hasClass("visible") == false){
            container_admin.addClass("visible");
        }
    });

    $("#close-admin").click(function(){
        setTimeout(() => container_admin.removeClass("visible"), 200);
    });

    $("#admin-dist-btn").click(function(){
        var dist = parseFloat($("#admin-dist").val());
        update_dist({distance: dist/10});
        socket.emit('update', {distance: (dist/10).toString()});
    });

    //////////////////////
    // DEBUG            //
    //////////////////////

    var run_inter = undefined;
    var run_debug = false;
    var dist_debug = 0.0;
    var speed_debug = $("#debug-speed").val();


    if (debug){
        $(".container-debug").css("display", "block");
        thresh_dist = 500;
    }

    function maj_debug(){
        dist_debug = dist_debug + speed_debug/10;
        $("#dist-val").html(Math.round(dist_debug));
        update_dist({distance: dist_debug.toString()});
    }

    $("#debug-speed").on("change", function(val){
        speed_debug = $("#debug-speed").val();
        $("#speed-val").html(speed_debug);
        console.log("update speed", speed_debug);
    });

    $("#debug-run").on("change", function(val){
        run_debug = $("#debug-run")[0].checked;
        console.log("run", run_debug);

        if (run_debug){
            run_inter = setInterval(maj_debug, 100);
        }else{
            clearInterval(run_inter);
        }
    });

    $("#debug-dist").on("change", function(val){
        dist_debug = parseFloat($("#debug-dist").val());
        $("#dist-val").html(Math.round(dist_debug));
        console.log("debug dist", run_debug);
    });

    $("#debug-dist-btn").on("click", function(val){
        dist_debug = parseFloat($("#debug-dist").val());
        $("#dist-val").html(Math.round(dist_debug));
        console.log("debug dist", run_debug);
    });

    var milestones_debug = [
        ["debut", 0],
        ["berlin", 73870],
        ["kiev", 189970],
        ["seattle", 1722390],
        ["montreal", 2104780],
        ["fin", 2663990]
    ];

    milestones_debug.forEach(i => {
        $("#debug-" + i[0]).click(function(){
            dist_debug = i[1];
            $("#dist-val").html(Math.round(dist_debug));
            $("#debug-dist").val(Math.round(dist_debug));
        });
    });


    //////////////////////
    // Socket com       //
    //////////////////////

    socket.on('connect', function(){
        socket.emit('message', {data: 'hello io'});
    });

    socket.on('message', function(data){
        console.log(data);
    });

    socket.on('hedge', function(data){
        console.log("hedge", data);

        update_dist(data);
    });


    //////////////////////
    // UPDATE STATES    //
    //////////////////////

    function update_label(){
        if (!one_turn){
            $(".label-container").each(function(d) {
                var op = 0;
                if (d>0 && d<milestone.length) {
                    if (milestone[d-1][0] <= cur_dist/1000.0){
                        op = 1;
                    }
                }else if (d == 0){
                    op = 1;
                }
                if (op == 1){
                    $("#label-container-"+d).removeClass("hidden");
                }else{
                    $("#label-container-"+d).addClass("hidden");
                }
            });
        }else{
        }
    }

    update_label();

    function update_place(){
        var loc_place = undefined;
        if (cur_dist == 0){
            cur_place = 0;
            return;
        }

        var loc_cur_dist = cur_dist;
        if (one_turn){
            loc_cur_dist = cur_dist%max_dist;
        }
        // if (pos_m >= this.tot_dist + 10){
        //     pos_m = pos_m % this.tot_dist;
        //     one_turn = true;
        //     // pos_m = this.tot_dist - 1;
        // }else if(pos_m > this.tot_dist){
        //     pos_m = this.tot_dist - 1;
        // }

        for (var i=1; i<milestone_data.length-1; i++){
            var l = milestone_data[i];
            // milestone.forEach(function(l, i){
            var d = l[0]*1000;
            if (loc_cur_dist >= d){
                if (loc_cur_dist < d + thresh_dist){
                    loc_place = i;
                }

                if (loc_cur_dist<milestone_data[i+1][0]*1000){
                    cur_place = i;
                    console.log("cur_place", cur_place);
                    break;
                }
            }
        }

        if (place != loc_place){
            place = loc_place;
            if (!one_turn){
                place_change();
            }else{
                ring.maj_label_anim(cur_place);
            }
        }
        return;
    }


    //////////////////////
    // SCENE EVENTS     //
    //////////////////////

    setInterval(function(){
        let now = new Date();
        if (now - last_hedge > idle_duration * 1000){
            if (idle == false){
                idle = true;
                anim_idle();
            }
        }
    }, 1000);

    var anim_pos = 0;
    var anim_inter = undefined;

    function anim_active(){
        clearInterval(anim_inter);
        $(".label-container").addClass("hidden");
        $(".label-anim-container").addClass("visible");
        $("#youpi").removeClass("hidden");

        ring.maj_label_anim(cur_place);
        anim_inter = setInterval(function(){
            ring.hide_labels = false;
            ring.draw_ring(cur_dist/1000, anim_pos);
            anim_pos += 0.02;
            if (one_turn){
                $(".label-container").addClass("hidden");
            }
            if (anim_pos > 1){
                anim_pos = 1;
                ring.draw_ring(cur_dist/1000, easeInOutQuad(anim_pos));
                if (one_turn){
                    $(".label-container").addClass("hidden");
                }
                clearInterval(anim_inter);
            }
        }, 20);
    }

    function stop_anim_active(){
        clearInterval(anim_inter);
        $("#youpi").addClass("hidden");
        $(".label-anim-container").removeClass("visible");
        // $(".label-container").removeClass("hidden");
        // show(".label-container");
        anim_inter = setInterval(function(){
            ring.hide_labels = true;
            ring.draw_ring(cur_dist/1000, anim_pos);
            anim_pos -= 0.01;
            if (anim_pos < 0){
                anim_pos = 0;
                ring.draw_ring(cur_dist/1000, easeInOutQuad(anim_pos));
                $("#label-container-"+cur_place).removeClass("hidden");
                clearInterval(anim_inter);

                if (one_turn){
                    $(".label-container").removeClass("hidden");
                }
            }
        }, 30);
    }


    function anim_idle(){
        if (state != "new_place" && !state_end){
            console.log("anim_idle");

            if (!one_turn){
                create_text($(".container-text"), textes["depart"]);
            }else{
                create_text($(".container-text"), textes["arrivee"]);
            }
            $('.container-text').removeClass("hidden");
            $('.container-text').removeClass("place");

            // update_label();
            // ring.draw_ring(cur_dist/1000.0, 0);

            $('.flipclock').removeClass("big");
            $('.flipclock').addClass("small");

            container_ring.addClass("visible");
            stop_anim_active();
        }
    }

    function stop_anim_idle(){
        if (state != "new_place" && !state_end){
            // clearInterval(lang_interval);

            $('.container-text').addClass("hidden");

            $('.flipclock').addClass("big");
            $('.flipclock').removeClass("small");

            container_ring.addClass("visible");
            // container_ring.removeClass("visible");
            anim_active();
        }
    }

    function anim(txt){
        console.log("anim");

        create_text($(".container-text"), txt);

        $('.container-text').removeClass("hidden");
        $('.container-text').addClass("place");

        $('.flipclock').removeClass("big");
        $('.flipclock').addClass("small");

        container_ring.addClass("visible");
        stop_anim_active();
    }

    function stop_anim(){
        clearInterval(lang_interval);

        $('.container-text').addClass("hidden");

        $('.flipclock').removeClass("small");
        $('.flipclock').addClass("big");

        anim_active();
    }

    function restart(){
        console.log("restart");
        if (idle){
            ring.draw_ring(0, 0);
            cur_dist = 0;
            state_end = false;
            flip.flip_num(cur_dist);
            socket.emit('update', {distance: "0"});
            update_place();
            anim_idle();
            $(".label-container").addClass("hidden");
            $("#label-container-0").removeClass("hidden");
            cur_dist = 0;
            // anim(textes["depart"]);
        }else{
            setTimeout(restart, 1000);
        }
    }

    var last_step = 0;
    var state_start = false;
    var state_end = false;

    function update_dist(data){
        last_hedge = new Date();

        // if (state_end){
        //     return "end";
        // }

        if (cur_dist == 0 && !state_start){
            state_start = true;
            play("/static/audio/depart.wav");
            setTimeout(function(){
                state_start = false;
            }, 48000);
        }

        cur_dist = Math.round(parseFloat(data['distance'])*10);
        $(".distance").text(data['distance']);

        if (cur_dist > max_dist){
            if (cur_dist < max_dist + 10000){
                state = "new_place";
            }else{
                console.log("state after end");
                state = "";
            }
        }
        if (cur_dist >= max_dist && !state_end && !one_turn){
            console.log("state end start");
            one_turn = true;
            // cur_dist = max_dist;
            update_place();
            cur_place = 0;
            flip.flip_num(cur_dist);
            anim(textes["arrivee"]);
            state_end = true;
            play("/static/audio/arrivee.wav");
            setTimeout(function(){
                state_end = false;
                console.log("end end");
                // restart();
            }, 50000);
        }

        if (anim_pos == 0 || anim_pos == 1){
            flip.flip_num(cur_dist);
            update_place();
        }



        if (idle){
            idle = false;
            stop_anim_idle();
            dist_idle = cur_dist;

            if (state_start == false && play_place == false){
                var num_audio = 1 + Math.floor(Math.random()*16);
                var num_clap = 1 + Math.floor(Math.random()*3);
                current_audio = play("/static/audio/" + num_audio + ".wav");
                current_audio = play("/static/audio/clap_" + num_clap + ".wav");
            }

            last_step = 0;
        }else{
            var dist_from_idle = cur_dist - dist_idle;
            if (!state_start && !play_place && !state_end){
                if (last_step%300 > dist_from_idle%300){
                    if (Math.floor(dist_from_idle/300)%4 == 0){
                        var num_clap = 1 + Math.floor(Math.random()*3);
                        current_audio = play("/static/audio/clap_" + num_clap + ".wav");
                    }else{
                        var num_audio = 1 + Math.floor(Math.random()*16);
                        current_audio = play("/static/audio/" + num_audio + ".wav");
                    }
                    console.log("ALLEZ ALLEZ ALLEZ", dist_from_idle);
                }
            }
            last_step = dist_from_idle;

            if (anim_pos == 1){
                ring.draw_ring(cur_dist/1000, 1);
            }
        }

        return "go";
    }

    function place_change(){
        console.log("place changed", place);

        if (place != undefined){

            update_place();
            update_label();

            var txt = create_text_place($(".container-text")) ;

            anim(txt);

            current_audio.pause();

            play_place = true;
            current_audio = play("/static/audio/etape_" + (place) + ".wav");
            current_audio.onended = function(){
                console.log("ended");
                play_place = false;
            };
            state = "new_place";


            $("#label-container-" + (place)).removeClass("hidden");
            $("#label-container-" + (place)).addClass("place");
            $("#label-" + (place)).addClass("place");

            ring.draw_ring(cur_dist/1000.0, 0);
        }else{
            state = "running";

            $(".label").removeClass("place");

            stop_anim();
            clearInterval(inter_place);
        }
    }


    anim_idle();
});
