
//////////////////////
// UTILS            //
//////////////////////

function dist(a, b){
    return Math.sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y));
}

function draw_path(ctx, p){
    if (p.t == "m"){
        ctx.moveTo(p.x, p.y);
    }else if (p.t == "l"){
        ctx.lineTo(p.x, p.y);
    }else if (p.t == "c"){
        ctx.arc(p.x, p.y, p.r, p.s, p.e, true);
    }
}

function range_lerp(x, min_x, max_x, min_y, max_y){
    return (x-min_x) * (max_y-min_y)/(max_x-min_x) + min_y;
}

function lerp(value1, value2, amount) {
	  amount = amount < 0 ? 0 : amount;
	  amount = amount > 1 ? 1 : amount;
	  return value1 + (value2 - value1) * amount;
};

//////////////////////
// MODULE           //
//////////////////////

function Ring(id_container, id_canvas, margin_in, radius_in, offset_in, milestones){

    this.container = document.getElementById(id_container);
    this.canvas = document.getElementById(id_canvas);

    this.margin = margin_in;
    this.radius = radius_in;
    this.offset = offset_in;
    this.milestones_data = milestones;

    this.milestones = [];
    this.tot_dist = 0;

    this.label_anim = {};

    this.ctx = undefined;

    this.pos_anim_min = -0.03;
    this.pos_anim_max = 0.2;

    this.hide_labels = false;

    //////////////////////
    // INIT             //
    //////////////////////

    this.init = function(){
        this.init_ctx();
        this.handle_milestones();
        this.pts = this.create_pts();
        this.create_pts_milestones();
    };

    this.init_ctx = function(){
        this.ctx = this.canvas.getContext("2d");
        this.ctx.lineWidth = 10;
        this.ctx.fillStyle = '#1559a0';
        this.ctx.strokeStyle = '#1559a0';
        this.ctx.lineCap = "round";
    };

    this.create_pts_milestones = function(){
        this.milestones.forEach((m,i) => {
            if (i<this.milestones.length-1){
                m.pt = this.get_pt_pos(m.pos);
            }else{
                m.pt = this.pts[9];
            }
        });
    };

    this.handle_milestones = function(){
        this.tot_dist = this.milestones_data[this.milestones_data.length-1][0];

        this.milestones_data.forEach(d => {
            this.milestones.push({
                dist: d[0],
                path_dist: d[1],
                pos: (d[1])/this.tot_dist,
                fr: d[2],
                en: d[3],
                nl: d[4],
            });
        });
    };

    this.create_pts = function(){
        var margin = this.margin;
        var radius = this.radius;

        var canvas_w = this.canvas.width;
        var canvas_h = this.canvas.height;

        var l_arc = radius * Math.PI/2;
        var l_l = canvas_w - 2*margin - 2*radius;
        var l_h = canvas_h - 2*margin - 2*radius;
        var l_tot = 2*l_l + 2*l_h + 4*l_arc;
        var l_offset = l_tot * this.offset/this.tot_dist;

        var l_cur = 0;
        var pts = [];

        pts.push({x: margin + radius + l_offset, y: canvas_h - margin, t: "m", d: l_cur});

        l_cur += l_l-l_offset;
        pts.push({x: canvas_w - margin - radius, y: canvas_h - margin, t: "l", d: l_cur});
        l_cur += l_arc;
        pts.push({x: canvas_w - margin - radius, y: canvas_h - margin-radius, r: radius, s: Math.PI/2, e: 0, t:"c", d: l_cur});

        l_cur += l_h;
        pts.push({x: canvas_w - margin, y: margin + radius, t:"l", d: l_cur});
        l_cur += l_arc;
        pts.push({x: canvas_w - margin - radius, y: margin + radius, r: radius, s: 0, e: 3*Math.PI/2, t:"c", d: l_cur});

        l_cur += l_l;
        pts.push({x: margin + radius, y: margin, t:"l", d: l_cur});
        l_cur += l_arc;
        pts.push({x: margin + radius, y: margin + radius, r: radius, s: 3*Math.PI/2, e: Math.PI, t:"c", d: l_cur});

        l_cur += l_h;
        pts.push({x: margin, y: canvas_h - margin - radius, t:"l", d: l_cur});
        l_cur += l_arc;
        pts.push({x: margin + radius, y: canvas_h - margin - radius, r: radius, s: Math.PI, e: Math.PI/2, t:"c", d: l_cur});

        l_cur += l_offset;
        pts.push({x: margin + radius + l_offset, y: canvas_h - margin, t: "l", d: l_cur});
        console.log("pts", pts);

        this.pos_anim_min = (200-l_offset)/l_tot;
        this.pos_anim_max = (pts[1].d-130)/l_tot;

        this.l_arc = l_arc;
        this.l_l = l_l;
        this.l_h = l_h;
        this.l_tot = l_tot;

        return pts;
    };

     this.create_pts_ref = function() {
         var pts_ref = [];
         this.pts.forEach(p => {
             pts_ref.push({x: p.x,
                           y: p.y,
                           d: p.d});
         });

         return pts_ref;
     };


    //////////////////////
    // GEOM             //
    //////////////////////

    this.get_act_pos = function(m){
        if (m<this.tot_dist){
            var i_m = 0;
            for (var i=0; i<this.milestones.length; i++){
                if (m > this.milestones[i].dist && m <= this.milestones[i+1].dist){
                    i_m = i;
                    break;
                }
            }

            if (m == 0) return 0;

            var reste = m - this.milestones[i_m].dist;
            var start_dist = this.milestones[i_m].dist;
            var end_dist = this.milestones[i_m+1].dist;
            var start_path_dist = this.milestones[i_m].path_dist;
            var end_path_dist = this.milestones[i_m+1].path_dist;

            var path_dist = start_path_dist + reste*(end_path_dist - start_path_dist)/(end_dist - start_dist);
            // console.log("act_pos", m, path_dist/this.tot_dist);
            return path_dist/this.tot_dist;
        }else{
            return 1;
        }
    };

    this.get_pt_arc = function(p, reste){
        var angle = reste / this.radius;
        var x = p.x + radius * Math.cos(p.s - angle);
        var y = p.y + radius * Math.sin(p.s - angle);
        return {x: x, y: y};
    };

    this.get_pt_pos = function(pos){
        var reste = pos*this.l_tot;
        var i_pt = -1;
        var pt_out = {};
        var pt_l = {};

        for (var i=0; i<this.pts.length; i++){
            if (this.pts[i].d >= pos*this.l_tot){
                reste = (this.pts[i].d - pos*this.l_tot) ;
                i_pt = i;
                break;
            }
        }

        // console.log("i_pt", i_pt, pos);

        pt_l = this.pts[i_pt];

        pt_out.x = pt_l.x;
        pt_out.y = pt_l.y;

        if (i_pt == 0){
            pt_out.x = pt_l.x + pos * this.l_tot;
        }else if (i_pt%2 == 0){
            pt_out = this.get_pt_arc(pt_l, this.l_arc - reste);
        }else if (i_pt == 1 || i_pt == 9){
            pt_out.x = pt_l.x - reste;
        }else if (i_pt == 3){
            pt_out.y = pt_l.y + reste;
        }else if (i_pt == 5){
            pt_out.x = pt_l.x + reste;
        }else if (i_pt == 7){
            pt_out.y = pt_l.y - reste;
        }

        pt_out.inv = false;

        if (pos<0.5){
            pt_out.inv = true;
        }
        return pt_out;
    };


    //////////////////////
    // DRAWING          //
    //////////////////////

    this.create_label = function(label, i_label){
        var label_container = document.createElement("div");
        label_container.classList.add("label-container");
        label_container.id = "label-container-" + i_label;
        label_container.style.left = label.pt.x + "px";
        label_container.style.top = label.pt.y + "px";

        var label_div = document.createElement("span");
        label_div.classList.add("label");
        label_div.id = "label-" + i_label;
        if (label.pt.inv){
            label_div.classList.add("inverse");
        }
        ["fr", "en", "nl"].forEach((l, i) => {
            var label_text = document.createElement("div");
            label_text.classList.add(l);
            label_text.innerHTML = label[l];
            label_div.appendChild(label_text);
        });

        label_container.appendChild(label_div);
        this.container.appendChild(label_container);
    };

    this.create_labels = function(){
        this.milestones.forEach((m, i) => {
            this.create_label(m, i);
        });

        var label_container = document.createElement("div");
        label_container.classList.add("label-anim-container");
        var pt_label = this.get_pt_pos(this.pos_anim_min);

        label_container.style.left = pt_label.x-350 + "px";
        label_container.style.top = pt_label.y + "px";

        var label_div = document.createElement("div");
        label_div.classList.add("label");
        ["fr", "en", "nl"].forEach((l, i) => {
            var label_text = document.createElement("div");
            label_text.classList.add(l);
            label_div.appendChild(label_text);
            this.label_anim[l] = label_text;
        });

        this.maj_label_anim(0);
        label_container.appendChild(label_div);
        this.container.appendChild(label_container);
    };

    this.maj_label_anim = function(i_label){
        var label = this.milestones[i_label];
        ["fr", "en", "nl"].forEach((l, i) => {
            this.label_anim[l].innerHTML = label[l];
        });
    };

    this.draw_path_pos = function(pos_start, pos){
        var reste = pos*this.l_tot;
        var i_pt = 0;
        var pt_out = {};

        this.ctx.beginPath();

        if (pos_start != 0){
            var pt_start = this.get_pt_pos(pos_start);
            this.ctx.moveTo(pt_start.x, pt_start.y);
        }else{
            draw_path(this.ctx, this.pts[0]);
        }

        for (var i=1; i<this.pts.length; i++){
            if (this.pts[i].d > pos*this.l_tot){
                reste = (this.pts[i].d - pos*this.l_tot) ;
                i_pt = i;
                break;
            }else{
                draw_path(this.ctx, this.pts[i]);
            }
        }

        var pt_l = this.pts[i_pt];
        // console.log(pos, i, pt_l);

        var pt_bonus = {};
        pt_bonus.x = pt_l.x;
        pt_bonus.y = pt_l.y;
        pt_bonus.t = pt_l.t;

        if (pt_l.t == "c"){
            pt_bonus.r = pt_l.r;
            pt_bonus.s = pt_l.s;
            pt_bonus.e = pt_l.s - (this.l_arc-reste)/this.radius;
        }else if (pt_l.t == "l"){
            if (i == 1 || i == 9){
                pt_bonus.x = pt_l.x - reste;
            }else if (i == 3){
                pt_bonus.y = pt_l.y + reste;
            }else if (i == 5){
                pt_bonus.x = pt_l.x + reste;
            }else if (i == 7){
                pt_bonus.y = pt_l.y - reste;
            }
        }

        draw_path(this.ctx, pt_bonus);
        this.ctx.stroke();
    };


    this.draw_ring = function(pos_m, anim){
        var pos_m_2 = pos_m;
        var one_turn = false;
        if (pos_m >= this.tot_dist + 10){
            pos_m = pos_m % this.tot_dist;
            one_turn = true;
            // pos_m = this.tot_dist - 1;
        }else if(pos_m > this.tot_dist){
            pos_m = this.tot_dist - 1;
        }

        // console.log("draw_ring", pos_m);
        var act_pos = this.get_act_pos(pos_m);

        this.ctx.strokeStyle = '#1559a0';
        this.ctx.clearRect(0, 0, 1920, 1080);
        if (!one_turn){
            this.ctx.strokeStyle = 'lightgray';
        }

        if (anim == 0){
            this.ctx.beginPath();
            this.ctx.lineWidth = 10;
            this.pts.forEach(p => draw_path(this.ctx, p));
            this.ctx.stroke();
        }else{
            this.ctx.lineWidth = 10 + 10 * anim;
            this.draw_path_pos(this.pos_anim_min*anim, 1 - anim*(1-this.pos_anim_max));
            this.ctx.beginPath();
            this.ctx.moveTo(this.pts[0].x, this.pts[0].y);
            this.ctx.lineTo(lerp(this.pts[0].x, 1920, anim), this.pts[0].y);
            this.ctx.stroke();
        }


        this.ctx.strokeStyle = '#1559a0';
        this.ctx.lineWidth = 15;

        var anim_pt_pos = act_pos;

        var i_milestone = 0;
        if (anim == 0){
            if (!one_turn){
                this.milestones.forEach((m,i) => {
                    if (i<this.milestones.length-1){
                        if (m.pos<=act_pos){
                            // document.getElementById("label-container-"+i)
                            //     .classList
                            //     .remove("hidden");
                            this.ctx.fillStyle = '#1559a0';
                            this.ctx.beginPath();
                            this.ctx.arc(m.pt.x, m.pt.y, 15, 0, 2* Math.PI);
                            this.ctx.fill();
                        }
                    }
                });
            }else{
                this.milestones.forEach((m,i) => {
                    if (i<this.milestones.length-1){
                        // document.getElementById("label-container-"+i)
                        //     .classList
                        //     .remove("hidden");
                        this.ctx.fillStyle = '#1559a0';
                        this.ctx.beginPath();
                        this.ctx.arc(m.pt.x, m.pt.y, 15, 0, 2* Math.PI);
                        this.ctx.fill();
                    }
                });
            }

        }else{
            var reste = 0;
            var ecart = 0;
            var next_milestone;
            var prev_milestone;
            for (var i=0; i<this.milestones.length-1;i++){
                if (pos_m > this.milestones[i].dist && pos_m < this.milestones[i+1].dist){
                    i_milestone = i;
                    next_milestone = this.milestones[i+1];
                    prev_milestone = this.milestones[i];
                    reste = pos_m - prev_milestone.dist;
                    ecart = next_milestone.dist - prev_milestone.dist;
                    break;
                }
            }
            var anim_pt_pos_rel = 0;


            if (ecart != 0){
                anim_pt_pos_rel = lerp(this.pos_anim_min, this.pos_anim_max, reste/ecart);
                anim_pt_pos = lerp(act_pos, anim_pt_pos_rel, anim);
                // console.log("anim_pt_pos", anim_pt_pos);

                var next_pos_rel = lerp(next_milestone.pos, this.pos_anim_max, anim);
                var pt_next = this.get_pt_pos(next_pos_rel);
                if (!one_turn){
                    this.ctx.fillStyle = 'lightgray';
                }
                this.ctx.beginPath();
                this.ctx.arc(pt_next.x, pt_next.y, 15*(1+anim), 0, 2* Math.PI);
                this.ctx.fill();

                var prev_pos_rel = lerp(prev_milestone.pos, this.pos_anim_min, anim);
                var pt_prev = this.get_pt_pos(prev_pos_rel);
                this.ctx.fillStyle = '#1559a0';
                this.ctx.beginPath();
                this.ctx.arc(pt_prev.x, pt_prev.y, 15*(1+anim), 0, 2* Math.PI);
                this.ctx.fill();

                var pt_start = this.get_pt_pos(this.pos_anim_min*anim);
                this.ctx.beginPath();
                this.ctx.arc(pt_start.x, pt_start.y, 15*(1+anim), 0, 2* Math.PI);
                this.ctx.fill();

                this.ctx.lineWidth = 15 + 15*anim;


                if (!one_turn){
                    this.milestones.forEach((m,i) => {
                        if (i<this.milestones.length-1){
                            if (m.pos <= anim_pt_pos){
                                if (this.hide_labels){
                                    document.getElementById("label-container-"+i)
                                        .classList
                                        .remove("hidden");
                                }
                                this.ctx.fillStyle = '#1559a0';
                                this.ctx.beginPath();
                                this.ctx.arc(m.pt.x, m.pt.y, 15, 0, 2* Math.PI);
                                this.ctx.fill();
                            }else{
                                if (this.hide_labels){
                                    document.getElementById("label-container-"+i)
                                        .classList
                                        .add("hidden");
                                }
                            }
                        }
                    });
                }else{
                    // this.milestones.forEach((m,i) => {
                    //     if (i<this.milestones.length-1){
                    //         document.getElementById("label-container-"+i)
                    //             .classList
                    //             .remove("hidden");
                    //     }
                    // });
                }
            }
        }

        var pt_pos = this.get_pt_pos(anim_pt_pos);

        var rad_plus = 1;
        if (anim != 0){rad_plus = 1+anim;}

        var pos_start_anim = this.pos_anim_min*anim;
        if (i_milestone > 0 && pos_m > this.milestones[1].dist + 5000){
            pos_start_anim = pos_start_anim*5;
        }
        this.draw_path_pos(pos_start_anim, anim_pt_pos);

        this.ctx.beginPath();
        this.ctx.arc(pt_pos.x, pt_pos.y, 20*rad_plus, 0, 2* Math.PI);
        this.ctx.fill();
        this.ctx.fillStyle = 'white';
        this.ctx.beginPath();
        this.ctx.arc(pt_pos.x, pt_pos.y, 14*rad_plus, 0, 2* Math.PI);
        this.ctx.fill();
        this.ctx.fillStyle = '#1559a0';
        this.ctx.beginPath();
        this.ctx.arc(pt_pos.x, pt_pos.y, 8*rad_plus, 0, 2* Math.PI);
        this.ctx.fill();
    };


    return this;
}
