from time import sleep
import requests
import random


def send(p):
    requests.get('http://localhost:5000/update', params=p)


cur_dist = 73870

params = {'distance': 0,
          'x': 0,
          'y': 0}


if __name__ == "__main__":
    while 1:
        send(params)
        cur_dist = round(random.random()*2 + 1,2)
        params['distance'] = cur_dist
        sleep(random.random()+0.2)
