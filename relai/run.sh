#!/bin/sh

python3 app.py &

sleep 10
chromium-browser --check-for-update-interval=31536000 --autoplay-policy=no-user-gesture-required --app="http://localhost:5000" --start-fullscreen&

sleep 4
python3 hedge.py &

