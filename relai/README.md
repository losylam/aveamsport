Relai
==

Dispositif technique pour l'installation "Passer le relai"

## Configuration

L'application est composé de deux scripts :
* `hedge.py` fait l'interface avec le modem marvelmind et calcule la distance parcouru
* `app.py` est l'application flask (interface web)

Sinon :
* La distance courante est stockée dans le fichier `cur_dist`.
* On peut définir le pas (=la distance minimale qui est considérée comme un déplacement) dans `config.py`
* `simul.py` est utilisé pour du développement et du test


## Fichier

Les fichiers audios doivent être placés dans le dossier `static/audio`


