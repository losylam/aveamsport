#!/bin/bash

BASEDIR=$(dirname "$0")
DISPOSITIF=`cat $BASEDIR/dispositif`

if [ $DISPOSITIF = "boxe" ]
then
	echo "start boxe"
	cd $BASEDIR/boxe
	sh run.sh&
elif [ $DISPOSITIF = "bascule" ]
then
	echo "bascule"
	cd $BASEDIR/bascule
	sh run.sh&
elif [ $DISPOSITIF = "photomaton" ]
then
	  echo "photomaton"
	  cd $BASEDIR/photomaton
	  sh run.sh&
elif [ $DISPOSITIF = "relai" ]
then
	  echo "relai"
	  cd $BASEDIR/relai
	  sh run.sh&
fi

xset s 0
xset -dpms

unclutter -idle 5&
