import subprocess
from datetime import date
import time
import sys

import keyboard

from gpiozero import Button, LED
from picamera import PiCamera
from escpos.printer import Usb

from PIL import Image

pin_button = 4
pin_flash = 26

# durée du flash en secondes
duration_flash = 1.3

# paramètres de traitement d'image
gamma = 1.0       # augmenter la luminosité (valeur max: 2.4)
rotate = False    # tourner l'image de 90 degres
crop = True       # couper les bords de l'image
crop_val = 200    # largeur de l'image à couper

# textes
# textes à imprimer sur le ticket
# une liste de 9 éléments (3 par langues)
# - entête en gras ("Bravo...")
# - corp du texte (liste à puce)
# - conclusion ("Motivé pour un second tour")
#
# respecter le nombre de caractères par lignes
# - 47 pour les entêtes et conclusions
# - 60 plus les deux espaces du début pour le corp du texte

textes = ["""
Bravo, vous avez terminé ces épreuves avec
succès ! Et vous avez rendu à votre corps un
fier service :
""", """
  - Vous venez de booster votre immunité : votre corps est
  désormais plus fort pour se protéger contre les agressions
  extérieures, et pour lutter contre l'apparition de maladies,
  comme le cancer par exemple.

  - Vous vous sentez bien ? C'est parce que, en faisant de
  l'exercice, vous avez libéré des hormones indispensables
  à votre bien être : de la sérotonine, qui régule l'humeur
  et l'anxiété et de l'endorphine, aussi appelée hormone du
  plaisir.
""", """
Motivés pour un second tour ? :)

""", """
Well done, you've completed these challenges
successfully! And you've done your body
a lot of good:
""", """
  - You've just boosted your immunity levels: your body is now
  better protected against external stress factors and better
  prepared to fight off diseases, such as cancer for example.

  - Feeling good? That's because you release hormones during
  exercise that are essential to your wellbeing: serotonin,
  which regulates our moods and anxiety levels and endorphins,
  also known as the pleasure hormone.
""", """
Ready to start again? :)

""", """
Bravo, u hebt deze tests met succes afgelegd! En
u hebt uw lichaam een geweldige dienst verleend:
""", """
  - U hebt uw immuniteit een boost gegeven: uw lichaam is
  voortaan sterker en kan zich dus beter beschermen tegen
  agressies van buitenaf en de strijd aanbinden tegen
  ziektes, zoals kanker.

  - Voelt u zich goed? Dat komt doordat uw lichaam dankzij
  lichaamsbeweging de hormonen produceert die u nodig hebt voor
  uw welzijn: serotonine, dat de stemming regelt en angstgevoel
  afremt, en endorfine, dat ook gelukshormoon wordt genoemd.
""", """
Wat dacht u van een tweede ronde? :)

"""]


def dither(fin, fout, gamma=1):
    com = ["convert",
           fin,
           "+contrast"]

    if rotate:
        com += ["-rotate",
                "90"]
    if crop:
        w = 1200 - 2*crop_val
        com += ["-crop",
                "%sx720+%s+0" % (w, crop_val)]

    com += ["-resize",
            "576x",
            "+repage",
            "-colorspace",
            "Gray",
            "-gamma",
            str(gamma),
            "-ordered-dither",
            "o8x8",
            fout]
    subprocess.call(com)


if __name__ == "__main__":
    button = Button(pin_button)
    flash = LED(pin_flash)
    flash.on()

    img = Image.open("overlay.png")


    camera = PiCamera()
    camera.hflip = True

    camera.resolution = (1200, 720)

    camera.start_preview(resolution=(800, 480))
    camera.add_overlay(img.tobytes(), format='rgba', size=img.size, layer=3)
    # camera.add_overlay(img.tobytes(), size=img.size)

    p = None
    try:
        p = Usb(0x4b8, 0x0e28)
        p.charcode("ca_french")
    except:
        pass

    cpt = 0

    def stop():
        global camera
        print("exit")
        camera.stop_preview()
        # quit()
        # exit()
        # sys.exit()
        raise SystemExit

    keyboard.add_hotkey('esc', stop)

    while True:
        button.wait_for_press()

        print("push")
        flash.off()
        time.sleep(duration_flash)

        camera.capture('/home/pi/images/image_%s.jpg' % cpt)

        time.sleep(0.5)
        flash.on()

        if p:
            p.image('titre.png')

            # p.set(align="center", font="a", text_type="U")
            # p.text("                 \n")
            # p.text(date.today().strftime("date : %d/%m/%Y\n\n"))

            dither('/home/pi/images/image_%s.jpg' % cpt,
                   '/home/pi/images/image_out.gif',
                   gamma)
            p.image("/home/pi/images/image_out.gif")

            for i in range(3):
                p.set(align="center", font="a", text_type="B")
                p.text(textes[i*3+0])

                p.set(align="left", font="b", text_type="NORMAL")
                p.text(textes[i*3+1])

                p.set(align="center", font="a", text_type="NORMAL")
                p.text(textes[i*3+2])

                p.set(align="center", font="a", text_type="NORMAL")
                p.text("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _\n")

            p.text("\n")
            p.image("youpi_300px.gif")
            p.cut()

    #camera.stop_preview()
