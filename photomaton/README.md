Photomaton
==

## Configuration 

- règle udev pour imprimante thermique epson : 
https://python-escpos.readthedocs.io/en/latest/user/installation.html#setup-udev-for-usb-printers

- créer le dossier `/home/pi/images`

```
sudo cp 99-escpos.rules /etc/udev/rules.d
```
## Dependencies

installer avec apt 
- imagemagick 

installer avec pip (python3)
- keyboard
- python-escpos
- gpiozero


## Fichier

Les fichiers utilisés sont:
- `titre.png`: titre du ticket (fichier source `titre.svg` )'
- `youpi_300px.gif`: youpi matin en bas du ticket
- `overlay.png`: titre de l'expo sur l'écran (fichier source `overlay.svg`)
