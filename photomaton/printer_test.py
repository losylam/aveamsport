import subprocess

from escpos.printer import Usb
from datetime import date

rotate = False
crop = True
crop_val = 200

textes = ["""
Bravo, vous avez terminé ces épreuves avec
succès ! Et vous avez rendu à votre corps un
fier service :
""", """
  - Vous venez de booster votre immunité : votre corps est
  désormais plus fort pour se protéger contre les agressions
  extérieures, et pour lutter contre l'apparition de maladies,
  comme le cancer par exemple.

  - Vous vous sentez bien ? C'est parce que, en faisant de
  l'exercice, vous avez libéré des hormones indispensables
  à votre bien être : de la sérotonine, qui régule l'humeur
  et l'anxiété et de l'endorphine, aussi appelée hormone du
  plaisir.
""", """
Motivés pour un second tour ? :)
""", """
Well done, you've completed these challenges
successfully! And you've done your body
a lot of good:
""", """
  - You've just boosted your immunity levels: your body is now
  better protected against external stress factors and better
  prepared to fight off diseases, such as cancer for example.

  - Feeling good? That's because you release hormones during
  exercise that are essential to your wellbeing: serotonin,
  which regulates our moods and anxiety levels and endorphins,
  also known as the pleasure hormone.
""", """
Ready to start again? :)
""", """
Bravo, u hebt deze tests met succes afgelegd! En
u hebt uw lichaam een geweldige dienst verleend:
""", """
  - U hebt uw immuniteit een boost gegeven: uw lichaam is
  voortaan sterker en kan zich dus beter beschermen tegen
  agressies van buitenaf en de strijd aanbinden tegen
  ziektes, zoals kanker.

  - Voelt u zich goed? Dat komt doordat uw lichaam dankzij
  lichaamsbeweging de hormonen produceert die u nodig hebt voor
  uw welzijn: serotonine, dat de stemming regelt en angstgevoel
  afremt, en endorfine, dat ook gelukshormoon wordt genoemd.
""", """
Wat dacht u van een tweede ronde? :)
"""]


def dither(fin, fout, gamma=1):
    com = ["convert",
           fin,
           "+contrast"]

    if rotate:
        com += ["-rotate",
                "90"]
    if crop:
        w = 1200 - 2*crop_val
        com += ["-crop",
                "%sx720+%s+0" % (w, crop_val)]

    com += ["-resize",
            "576x",
            "+repage",
            "-colorspace",
            "Gray",
            "-gamma",
            str(gamma),
            "-ordered-dither",
            "o8x8",
            fout]
    subprocess.call(com)


if __name__ == "__main__":
    p = Usb(0x4b8, 0x0e28)
    p.charcode("ca_french")
    # p.text("Hello !\n\n")

    p.image('titre.png')

    p.set(align="center", font="a", text_type="U")
    p.text("                 \n")
    p.text(date.today().strftime("date : %d/%m/%Y\n\n"))

    dither('image.jpg', 'image_test.gif', 2.4)
    p.image('image_test.gif')

    for i in range(3):
        p.set(align="center", font="a", text_type="B")
        p.text(textes[i*3+0])

        p.set(align="left", font="b", text_type="NORMAL")
        p.text(textes[i*3+1])

        p.set(align="center", font="a", text_type="NORMAL")
        p.text(textes[i*3+2])

        p.set(align="center", font="a", text_type="U2")
        p.text("                          \n")
    p.text("\n")
    p.image("youpi_300px.gif")
    p.cut()


# while True:
#     button.wait_for_press()
#     print("push")
#     camera.capture('/home/pi/image.jpg') 
#     button.wait_for_release()
#     print("release")
#     dither('/home/pi/image.jpg', '/home/pi/image_out.gif')
#     p.text("Hello !\n\n")
#     p.image('/home/pi/image_out.gif')
#     p.text('\n')
#     p.cut()
#     #camera.stop_preview()
