# AveamSport

## Installation

- copier le dossier `aveamsport-[HASH]` dans le dossier utilisateur `/home/pi`
et le renommer `aveamsport`.

- exécuter le script d'installation:
```
cd /home/pi/aveamsport
sh install.sh
```

Ce script se charge de télécharger les dépendances et de configurer le démarrage automatique de l'application 

## Configuration globale


### Choix du dispositif

Créer un fichier nommé `dispositif` contenant uniquement le nom du dispositif à lancer : `relai`, `photomaton`, `bascule` ou `boxe`



## Dépendanca
Dépendances globales:
- python3
- python3-pip
- unclutter
- imagemagick

Dépendances python:
- flask
- flask_socketio
- eventlet
- gpiozero
- keyboard
- python-escpos
- crcmod
