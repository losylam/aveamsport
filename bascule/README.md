# Bascule (Garder le cap)

## Config raspberry pi

- changer la sortie audio pour le jack 3.5mm : `sudo raspi-config` puis > advanced settings
- résolution de l'écran : `Préférences` > `Configuration du raspberrypi` > `Display` > `Set resolution` > choisir `DMT mode 82 1920x1080 60Hz 16:9`


## Config chromium

autoriser l'autoplay sans interaction préalable de l'utilisateur :
```
chromium-browser --autoplay-policy=no-user-gesture-required
```

voir [ici](https://developers.google.com/web/updates/2017/09/autoplay-policy-changes)

## Fichiers

Les fichiers audios (applaudissements, tips, phrases d'introduction et de départ) sont à placer dans le dossier `static/audio`
