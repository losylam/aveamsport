$( document ).ready(function() {
    console.log('ping bascule');

    //////////////////////
    // Config           //
    //////////////////////

    // temps de jeu max (en secondes)
    var end_timeout = 120;

    // durée d'affichage des drapeaux après choix de la langue
    var duration_langues = 5;

    // durée du texte d'introduction
    var duration_intro = 46;

    // durée d'affichage du texte de consigne
    var duration_consigne = 8;

    // durée d'affichage du temps final
    var duration_record = 7;

    // durée d'affichage de l'écran "recommencer"
    var duration_restart = 7;

    // interval entre les conseils audios (tips)
    var timeout_tip = 5;

    // textes et traductions
    var textes = {
        consigne: {
            EN: "Let go of the bar when you hear the beep !",
            FR: "Au bip, lâche la barre pour tenir en équilibre !",
            NL: "Als je de biep hoort, laat je de afsluiting los !"
        },
        restart: {
            EN: "Press a button to start over",
            FR: "Appuie sur un bouton pour recommencer",
            NL: "Druk op een knop on opnieuw te beginnen"
        },
        oldrecord: {
            EN: "The record is ",
            FR: "Le record à battre est de ",
            NL: "Het te verslaan record is "
        },
        record: {
            EN: "New record !",
            FR: "Nouveau record !",
            NL: "Nieuw record !"
        },
        norecord: {
            EN: "The record was not broken",
            FR: "Le record n'a pas été battu",
            NL: "Het record werd niet verbroken"
        }
    } ;

    var test = false;
    if (test){
        duration_intro = 6;
        duration_consigne = 2;
        duration_langues = 2;
        duration_restart = 4;
        duration_record = 4;
    }

    //////////////////////
    // Init             //
    //////////////////////

    var state = "waiting";

    var chrono_audio = new Audio('/static/audio/chrono.wav');
    var tip_audio;

    var record = 0;
    var cur_time = 0;
    var chrono_interval = undefined;
    var restart_timeout = undefined;
    var forced_end_timeout = undefined;
    var countdown_interval = undefined;

    var lang = "";

    var cur_tip = 1;

    var bascule = false;

    var youpi = $(".youpi");
    var consigne = $(".consigne");
    var langues = $(".flags");
    var overlay = $(".overlay");
    var text_content = $(".text-content");
    var chrono = $(".chrono");
    var chrono_content = $(".chrono-content");

    var clock = document.getElementById("clock");

    var clock_rad = 320;
    var clock_stroke = 15;

    clock.setAttribute("width", clock_rad);
    clock.setAttribute("height", clock_rad);

    var ctx = clock.getContext("2d");
    ctx.lineWidth = clock_stroke;
    ctx.strokeStyle = '#1559a0';
    ctx.lineCap = "round";



    //////////////////////
    // Socket com       //
    //////////////////////

    var socket = io();
    socket.on('connect', function(){
        socket.emit('message', {data: 'hello io'});
    });

    socket.on('message', function(data){
        console.log(data);
    });

    socket.on('perdu', function(data){
        console.log("perdu");
        bascule = true;

        if (state == "chrono"){
            end_chrono();
        }
    });

    socket.on('ok', function(data){
        console.log("ok");
        bascule = false;
    });

    socket.on('button', function(data){
        console.log("button", data);
        if (state == "waiting"){
            choix_langue(data);
        }else if (state == "end"){
            replay();
        }
    });



    //////////////////////
    // Test/debug       //
    //////////////////////

    $('body').click(function(){
        if (state == "waiting"){
            choix_langue(["FR", "EN", "NL"][Math.floor(Math.random()*3)]);
        }else if (state == "chrono"){
            end_chrono();
        }else if (state == "end"){
            replay();
        }
    });



    //////////////////////
    // Utils            //
    //////////////////////

    function show(frame){
        frame.animate({opacity: 1});
    }

    function hide(frame){
        frame.animate({opacity: 0});
    }

    function draw_arc(angle){
        let r0 = clock_rad/2;
        let r = (clock_rad-clock_stroke)/2;
        ctx.clearRect(0, 0, clock_rad, clock_rad);
        ctx.beginPath();
        ctx.arc(r0, r0, r, -Math.PI/2,  (-Math.PI/2)+angle%(2*Math.PI));
        ctx.stroke();
    }

    Math.lerp = function (value1, value2, amount) {
	      amount = amount < 0 ? 0 : amount;
	      amount = amount > 1 ? 1 : amount;
	      return value1 + (value2 - value1) * amount;
    };

    function play_tip(){
        if (state == 'chrono'){
            if (cur_tip < 10){
                tip_audio = new Audio('/static/audio/tip_' + cur_tip + '_' + lang + '.wav');
                tip_audio.play();
                cur_tip += 1;
                setTimeout(play_tip, timeout_tip * 1000);
            }
        }
    }



    //////////////////////
    // Scenes           //
    //////////////////////

    function choix_langue(lang_in){
        state = "intro";
        lang = lang_in;

        var audio = new Audio('/static/audio/intro_'+lang+'.wav');
        audio.play();

        ["FR", "EN", "NL"].forEach(e => {
            if (e != lang){
                hide($(".flag-"+e));
            }
        });

        setTimeout(function(){
            hide(langues);
            show(youpi);
        }, duration_langues * 1000);

        setTimeout(function(){
            hide(youpi);
            show_consigne();
        }, (duration_intro - duration_consigne) * 1000);

        setTimeout(function(){
            hide(overlay);
            audio.pause();
	          start_countdown();
        }, duration_intro * 1000);
    // }, 46000);
    }

    function show_consigne(){
        consigne.html("");
        var new_div = $("<div/>", {class: lang}).html(textes["consigne"][lang]).appendTo(consigne);
        show(consigne);
    }

    function show_texte(texte){
        text_content.html("");
        var new_div = $("<div/>", {class: lang}).html(texte).appendTo(text_content);
        show(text_content);
    }

    function start_countdown(){
        state = "countdown";

        hide(overlay);
        hide(consigne);

        setTimeout(function(){
            show(chrono);
            show(chrono_content);
        }, 599);

        var audio = new Audio('/static/audio/depart_'+lang+'.wav');
        audio.play();

        if (record > 0){
            show(text_content);
            show_texte(textes["oldrecord"][lang] + record + " s.");
        }

        chrono_content.html("3");
        draw_arc(Math.PI*2-0.01);
        setTimeout(function(){
            var start_time = new Date();
            chrono_content.css("background-color", "transparent");
            chrono_content.css("color", "#1559a0");
            chrono_content.css("opacity", "1");
            // show(chrono_content);
            countdown_interval = setInterval(function(){
                var now = new Date() - start_time;
                draw_arc(Math.PI*2*(1-now/(3*1000.0)));
                cur_time = Math.floor(now/1000);

                chrono_content.text((3 - cur_time).toString());
            }, 50);

            setTimeout(function(){
                clearInterval(countdown_interval);
                ctx.clearRect(0, 0, clock_rad, clock_rad);

                chrono_content.html(0);

                setTimeout(function(){
                    start_chrono();
                },500);
            }, 3000);
        }, 1000);
    }


    function start_chrono(){
        state = "chrono";

        hide(text_content);
        hide(overlay);
        show(chrono);

        chrono_audio.currentTime = 0;
        chrono_audio.play();

        cur_time = 0;

        if (bascule){
            end_chrono();
        }else{
            setTimeout(play_tip, timeout_tip * 1000);

            var start_time = new Date();

	          forced_end_timeout = setTimeout(end_chrono, end_timeout*1000 + 100);

            chrono_interval = setInterval(function(){
                var now = new Date() - start_time;
                cur_time = Math.floor(now/1000);
                chrono_content.html(cur_time);
            }, 500);
        }
    }


    function end_chrono(){
        state = "endd";

	      clearTimeout(forced_end_timeout);
        clearInterval(chrono_interval);
        chrono_audio.pause();
        cur_tip = 1;

        show(text_content);

        if (cur_time > record){
            record = cur_time;
            var audio = new Audio('/static/audio/record.wav');
            audio.play();
            show_texte(textes["record"][lang]);
        }else{
            var audio = new Audio('/static/audio/applaudissement_'+ (Math.floor(Math.random()*3) + 1) +'.wav');
            audio.play();
            show_texte(textes["norecord"][lang]);
        }

        setTimeout(function(){
            state = "end";
            hide(text_content);
            hide(chrono);
            hide(chrono_content);

            restart_timeout = setTimeout(restart, duration_restart * 1000);

            setTimeout(function(){
                show($(".replay"));
                show_texte(textes["restart"][lang]);
            }, 1000);
        }, duration_record * 1000);

    }

    function replay(){
        state = "replay";

        show(overlay);
        // hide(chrono_content);
        // hide(chrono);
        hide($(".replay"));

        clearTimeout(restart_timeout);
        setTimeout(start_countdown, 2000);
    }

    function restart(){
        show(overlay);
        hide(chrono);
        hide(chrono_content);
        hide(text_content);
        hide($(".replay"));

        setTimeout(function(){
            hide(overlay);
            show(langues);
            hide(consigne);
            $(".flag").css("opacity", 1);
            text_content.html("3");
            state = "waiting";
        }, 1000);
    }

    restart();
});
