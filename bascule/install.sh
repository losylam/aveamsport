#!/bin/bash

sudo apt-get install libusb-1.0.0-dev python3-pip python3-virtualenv

wget https://www.phidgets.com/downloads/phidget22/libraries/linux/libphidget22.tar.gz
tar -xvf libphidget22.tar.gz
rm libphidget22.tar.gz

mv libphidget22* libphidget22

cd libphidget22
./configure && make && sudo make install

sudo cp plat/linux/udev/99-libphidget22.rules /etc/udev/rules.d

cd ..

python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt



