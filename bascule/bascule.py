import time
import requests
import os
import threading

from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
import eventlet

# from Phidget22.Devices.Accelerometer import Accelerometer
# from Phidget22.PhidgetException import PhidgetException

# import pygame

from gpiozero import Button
rpi_ok = True

try:
    import RPi.GPIO
except (RuntimeError, ModuleNotFoundError):
    rpi_ok = False


app = Flask(__name__)
app.config['SECRET_KEY'] = "c'est un secret!"
socketio = SocketIO(app)

##################
# Configuration ##
##################

# logfile = time.strftime("%Y-%m-%d") + ".log"
logfile = None
debug = False

pin_FR = 20
pin_EN = 21
pin_NL = 19

pin_bascule = 5

bascule_threshold = 0.007
move_threshold = 0.0027
idle_time = 5

bascule_threshold_fix = 0.997

##################
# Init          ##
##################

# sound_file = {}

# pygame.mixer.init(44100, -16, 2, 512)
# for filepath in os.listdir("audio"):
#     if filepath.endswith(".wav"):
#         sound_file[filepath[:-4]] = pygame.mixer.Sound(os.path.join("audio", filepath))

bascule_center = 1.0
bascule = False
bascule_init = False

last_move = time.time()
last_val = 1.0
idle = False


##################
# Flask         ##
##################


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/perdu')
def perdu():
    socketio.emit("perdu")
    return "ok"


@app.route('/ok')
def ok():
    socketio.emit("ok")
    return "ok"


@app.route('/idle/<int:state>')
def idle_send(state):
    if state == 1:
        socketio.emit("idle")
        # print("send idle")
    elif state == 0:
        socketio.emit("notidle")
        # print("send not idle")
    return "ok"


@app.route('/button')
def button():
    val = request.args.get("val")
    print("lang: ", val)
    socketio.emit("button", data=val)
    return "ok"


@socketio.on('message')
def handle_message(message):
    # print('received message: ',)
    # print(message)
    emit('message', 'hello from flask')


# @socketio.on('playy')
# def handle_play(data):
#     sound_filename = ""
#     if data["sound"] in ["bip", "clap", "loose"]:
#         sound_filename = data["sound"]
#     else:
#         sound_filename = "%s_%s" % (data["sound"], data["lang"])

#     # sound_file[sound_filename].play()


##################
# GPIO          ##
##################


def send_1():
    val = "FR"
    print("sent_button", val)
    requests.get('http://localhost:5000/button', params={"val": val})


def send_2():
    val = "EN"
    print("sent_button", val)
    requests.get('http://localhost:5000/button', params={"val": val})


def send_3():
    val = "NL"
    print("sent_button", val)
    requests.get('http://localhost:5000/button', params={"val": val})

def send_bascule_pressed():
    print("send_perdu")
    requests.get('http://localhost:5000/perdu')

def send_bascule_released():
    print("send_ok")
    requests.get('http://localhost:5000/ok')


##################
# Phidget       ##
##################


def checkBascule(acceleration):
    global bascule
    global last_move
    global last_val
    global idle

    #print(bascule, idle, acceleration)

    if abs(acceleration - last_val) > move_threshold:
        last_move = time.time()
        if idle:
            idle = False
            # print("not idle", acceleration, last_val)
            requests.get('http://localhost:5000/idle/0')

    last_val = acceleration

    print(bascule, idle, acceleration)
    #if abs(bascule_center - acceleration) > bascule_threshold:
    if acceleration < bascule_threshold_fix:
        if not bascule:
            bascule = True
            requests.get('http://localhost:5000/perdu')
    else:
        if bascule:
            bascule = False
            requests.get('http://localhost:5000/ok')


def onAccelerationChange(self, acceleration, timestamp):
    global bascule_init
    global bascule_center

    if debug:
        print("Acceleration: %s\t%s\t%s" % (acceleration[0],
                                            acceleration[1],
                                            acceleration[2]))

    if logfile:
        with open(logfile, "a") as f:
            f.write("%s; %s; %s; %s\n" % (time.time(),
                                          acceleration[0],
                                          acceleration[1],
                                          acceleration[2]))

    # print(bascule)
    if not bascule_init:
        bascule_init = True
        bascule_center = acceleration[2]
        #accelerometer0.setAccelerationChangeTrigger(0.001)

    checkBascule(acceleration[2])


def check_move():
    global last_move
    global idle

    while 1:
        # print("idle time", idle, time.time() - last_move)
        # print("check move")
        if not idle:
            if time.time() - last_move > idle_time:
                r = requests.get('http://localhost:5000/idle/1')
                idle = True
        time.sleep(1)


if __name__ == "__main__":
    if rpi_ok:
        button1 = Button(pin_FR)
        button1.when_pressed = send_1
        button2 = Button(pin_EN)
        button2.when_pressed = send_2
        button3 = Button(pin_NL)
        button3.when_pressed = send_3
        button_bascule = Button(pin_bascule)
        button_bascule.when_pressed = send_bascule_pressed
        button_bascule.when_released = send_bascule_released

    # idle_thread = threading.Thread(target=check_move)
    # idle_thread.start()

    # accelerometer0 = None
    # try:
    #     if accelerometer0 is None:
    #         print("setup accelerometer")
    #         accelerometer0 = Accelerometer()
    #         accelerometer0.openWaitForAttachment(5000)
    #         accelerometer0.setOnAccelerationChangeHandler(onAccelerationChange)
    #         accelerometer0.setDataInterval(100)
    #         print(accelerometer0.getDataInterval())
    # except PhidgetException:
    #     print("phidget setting failed")
    #     pass
    socketio.run(app, debug=False)
    # socketio.run(app)
    # accelerometer0.close()
